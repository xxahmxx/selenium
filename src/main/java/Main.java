import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Main {


    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.firefox.marionette", String.valueOf(Main.class.getResource("geckodriver.exe")
                .getPath()));
        WebDriver driver = new FirefoxDriver();

        String pageUrl = String.valueOf("file://" + Main.class.getResource("index.html").getPath());
        driver.get(pageUrl);

        JavascriptExecutor js = (JavascriptExecutor)driver;
        Object result = js.executeScript("return window.BestObjectEver === undefined");
        System.out.println(result);

        driver.wait(5000);
        driver.quit();

   }
}
